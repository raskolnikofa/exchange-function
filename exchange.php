<?php 

// 1 way

function conversion($uan){
$eur=$uan/30.3;
$usd=$uan/25.8;
$result="EUR:".round($eur, 2)."<br/>";
$result.="USD:".round($usd, 2)."<hr/>";
return ($result);
}

$uan=1000;
while ($uan<=2000) {
    echo "UAN:".$uan."<br/>";
    echo conversion($uan);
    $uan+=50;
}

// 2 way

/*
function conversion($uan){
$eur=$uan/30.3;
echo "EUR:".round($eur, 2)."<br/>";
$usd=$uan/25.8;
echo "USD:".round($usd, 2)."<hr/>";
}

$uan=1000;
while ($uan<=2000) {
    echo "UAN:".$uan."<br/>";
    conversion($uan);
    $uan+=50;
}
*/

// 3 way


/*function conversion($uan){
	while ($uan<=2000) {
	    echo "UAN:".$uan."<br/>";
	$eur=$uan/30.3;
	echo "EUR:".round($eur, 2)."<br/>";
	$usd=$uan/25.8;
	echo "USD:".round($usd, 2)."<hr/>";
	$uan+=50;}
}

conversion(1000);*/

?>